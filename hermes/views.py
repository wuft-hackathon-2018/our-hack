import datetime
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.http import Http404, HttpResponse, HttpResponseRedirect
def homepage(request):
    '''provides user with name and date'''

    first_name = 'guest'
    now = datetime.datetime.now()
    return render(request, 'homepage.html', {'current_date': now, 'first_name': first_name})