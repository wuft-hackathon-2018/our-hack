import csv
#missing something crucial from masters (randomized services data)
#should be ~ separated not comma separated
def cross_sell(userdata, k, reqnum):
    SERVICES = ["Mortage", "Auto loan", "Student loan", "Retirement", "College funds", "Credit cards", "General investment", "Personal loan", "Business loan", "Stock/trading account"]
    mcsv = list(csv.reader(open("master.csv", "r"), delimiter=","))
    dists = []
    for datum in mcsv:
       dist = 0.0
       for i in range(1, 9):
           if i == 4:
                continue
            #print(i)
           dist += abs(float(datum[i])-float(userdata[i]))
       dists += [[datum, dist]]
    nearest_neighbors = sorted(dists, key=lambda x:x[1])[:k]
    whattocallthis = []
    for i in range(10,19):
        score = 0
        for j in range(k):
            score += int(nearest_neighbors[j][0][i])
        whattocallthis += [[score, SERVICES[i-10]]]
    swtct = sorted(whattocallthis, key=lambda x:x[0], reverse=True)[:reqnum]
    retval = ""
    for eh in swtct:
        retval += "Recommend cross-selling "+str(eh[1])+" score "+str(eh[0])+"\n"
    return retval
dummy = ["USER_101",113.8580086,11501.37855,698,{'WITHDRAWAL': 33, 'DEPOSIT': 185, 'ATM Credit': 308, 'ATM Debit': 86, 'ONLINE': 70, '': 16},68,1,1,2,2,1,1,0,0,1,0,1,1,0]
print(cross_sell(dummy, 3, 3))
