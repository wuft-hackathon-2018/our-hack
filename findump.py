import csv
with open("transaction.csv", "r") as csvfile:
    finreader = list(sorted(csv.reader(csvfile, delimiter="\t"), key=lambda x:x[8])) #2.4M is slow
    usnum = 0
    amt = 0.0
    bal = 0.0
    typedict = {}
    customers = []
    j = 0
    for i in range(2, len(finreader)):
        try:
            amt += float(finreader[i][1])
        except:
            break #because jakob is lazy af
        bal += float(finreader[i][2])
        typedict[finreader[i][3]] = typedict.get(finreader[i][3], 0)+1 #keeps track of how many of each type of comment
        usnum += 1.0
        if(finreader[i][8] != finreader[i-1][8]):
            customers += [["USER_"+str(finreader[i-1][8]), amt/usnum, bal/usnum, sum(typedict.values()), str(typedict)]]
            bal = 0.0
            amt = 0.0
            usnum = 0
            typedict = {}
        j += 1
    csv.writer(open("refined.csv", "w"), delimiter=",").writerows(customers)